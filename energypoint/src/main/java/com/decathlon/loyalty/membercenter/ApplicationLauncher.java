package com.decathlon.loyalty.membercenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableDiscoveryClient
public class ApplicationLauncher {

    public static void main(String[] args){
        ConfigurableApplicationContext ctx = SpringApplication.run(ApplicationLauncher.class);
    }
}
