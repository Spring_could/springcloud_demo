package com.decathlon.loyalty.membercenter.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("api/v1/benefits/*")
public class EnergyPointController {

   @RequestMapping(value="balance",produces= MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
   public long balance(@RequestParam String cardnumber){
      return new Random().nextLong();
   }

   @RequestMapping(value="energy/{id}",produces= MediaType.APPLICATION_JSON_VALUE, method= RequestMethod.GET)
   public List points(@PathVariable(value = "id") String id, @RequestParam(required = false) String cardnumber, @RequestParam(required = false) String mobile){
       List<Object> points = new ArrayList<Object>();
       points.add(id!=null?"id : " + id : "id is null_server2");
       points.add(cardnumber != null?"cardnumber : "+cardnumber:"cardnumber is null_server1");
       points.add(mobile != null?"mobile : "+mobile:"mobile is null_server1");
       return points;
   }

   @RequestMapping(value="coupons", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
   public List coupon(){
       List<Object> coupons = new ArrayList<Object>();
       int number = new Random().nextInt(8);
       for(int i=1; i <= number; i++) {
           coupons.add("coupon "+i+": " + new Random().nextInt(50) + "; expired data: 2018-9-9 _server1");
       }
       return coupons;
   }

}
