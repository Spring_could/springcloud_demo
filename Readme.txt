This is a demo to should basic practice about Spring cloud, which including Eureka, Feign, Ribbon and Hystrix.
Eureka: server center, to show and manage all the service
Feign: declartive client(this demo shows different usage between Feign and RestTemplate)
Ribbon: load balance
Hystrix: service protection: isolation, downgrade, broker


Run this demo with steps:
1.  run  project: eurekaserver;
2.  run  peoject: energypoint;
3.  modify port and log(server1 and server2), run peoject: energypoint again;
4.  run project: member center

then:
1. you can visit: http://localhost:8090/, this is eureka server, we can see 4 server have register in it.
2. you can visit:
http://localhost:8092/api/v1/membercenter/membercenter_hystrix/33?cardnumber=19890502&mobile=888899990000
to see the effect of Hystrix
3. you can visit again and again:
http://localhost:8092/api/v1/membercenter/membercenter_feign/33?cardnumber=19890502&mobile=888899990000
to see the effect: Feign(declartive Rest client) and Ribbon(loadbalance) 


Next step:
1. enrich Hystrix define
2. define different eureka region and zone
3. enable hystrix dashboard
4. add zuul(gateway)
5. define different load balance strategy in Ribbon



