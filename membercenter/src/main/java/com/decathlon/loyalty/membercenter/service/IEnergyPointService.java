package com.decathlon.loyalty.membercenter.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name="energy-point-server")
public interface IEnergyPointService {

    @RequestMapping(value="api/v1/benefits/balance",produces= MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public long balance(@RequestParam("cardnumber") String cardnumber);

    @RequestMapping(value="api/v1/benefits/energy/{id}",produces= MediaType.APPLICATION_JSON_VALUE, method= RequestMethod.GET)
    public List points(@PathVariable("id") String id, @RequestParam(value="cardnumber", required = false) String cardnumber, @RequestParam(value="mobile", required = false) String mobile);

    @RequestMapping(value="api/v1/benefits/coupons", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public List coupon();
}
/**
 * here is tricky, every param need to set property: "value="" "
 */
