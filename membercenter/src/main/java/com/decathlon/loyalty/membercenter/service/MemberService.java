package com.decathlon.loyalty.membercenter.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class MemberService {

    @Autowired
    private RestTemplate template;

    @HystrixCommand(
            fallbackMethod = "doFallback",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000")
            },
            threadPoolProperties = {
                    @HystrixProperty(name = "keepAliveTimeMinutes", value = "2"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000")
            }
    )
    public List membercenter(String id, String cardnumber, String mobile){
        List<Object> list = new ArrayList<Object>();
        //url is wrong, here will be runtimeexception
        long balance =  template.getForEntity("http://energy-point-server/api/v1/points/balance",Long.class).getBody();
        List points = template.getForEntity("http://energy-point-server/api/v1/points/energypoint/33?cardnumber="+cardnumber+"&mobile="+mobile, List.class).getBody();
        List coupons = template.getForEntity("http://energy-point-server/api/v1/points/coupons",List.class).getBody();

        list.add(balance);
        list.add(points);
        list.add(coupons);
        return list;
    }

    public List doFallback(String id, String cardnumber, String mobile) {
        List<Object> list = new ArrayList<Object>();
        list.add("balance: unknown_for test");
        list.add("points: unknown");
        list.add("coupons: unknown");
        return list;
    }
}
