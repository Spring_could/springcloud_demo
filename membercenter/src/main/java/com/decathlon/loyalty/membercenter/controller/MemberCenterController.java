package com.decathlon.loyalty.membercenter.controller;

import com.decathlon.loyalty.membercenter.service.IEnergyPointService;
import com.decathlon.loyalty.membercenter.service.MemberService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/membercenter/*")
public class MemberCenterController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private IEnergyPointService template;

    @RequestMapping(value = "membercenter_hystrix/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public List membercenter(@PathVariable String id, @RequestParam(required = false) String cardnumber,
            @RequestParam(required = false) String mobile) {
        List list = memberService.membercenter(id, cardnumber, mobile);
        return list;
    }

    @RequestMapping(value = "membercenter_feign/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public List membercenter_test(@PathVariable String id, @RequestParam("cardnumber") String cardnumber,
            @RequestParam(required = false) String mobile) {
        List<Object> list = new ArrayList<Object>();
        List points = template.points(id, cardnumber, mobile);
        List coupons = template.coupon();

        long balance = template.balance(cardnumber);
        list.add(balance);
        list.add(points);
        list.add(coupons);
        return list;
    }


    @HystrixCommand(
            fallbackMethod = "doFallback",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000"),
            },
            threadPoolProperties = {
                    @HystrixProperty(name = "keepAliveTimeMinutes", value = "2"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000")
            }
    )
    @RequestMapping(value = "membercenter_feign_hystrix/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public List membercenter_test1(@PathVariable String id, @RequestParam(required = false) String cardnumber,
            @RequestParam(required = false) String mobile) {
        List<Object> list = new ArrayList<Object>();
        List points = template.points(id, cardnumber, mobile);
        List coupons = template.coupon();

        long balance = template.balance(cardnumber);
        list.add(balance);
        list.add(points);
        list.add(coupons);
        return list;
    }

    public List doFallback(String id, String cardnumber, String mobile) {
        List<Object> list = new ArrayList<Object>();
        list.add("balance: unknown");
        list.add("points: unknown");
        list.add("coupons: unknown");
        return list;
    }
}
