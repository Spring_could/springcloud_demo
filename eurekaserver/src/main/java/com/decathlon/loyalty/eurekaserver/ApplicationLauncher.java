package com.decathlon.loyalty.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableEurekaServer
public class ApplicationLauncher {

    public static void main(String[] args){
        ConfigurableApplicationContext ctx = SpringApplication.run(ApplicationLauncher.class);
    }
}
